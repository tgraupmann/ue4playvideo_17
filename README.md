# Project Files

![image_1](images/image_1.png)

# Widget Blueprint - HUD

![image_2](images/image_2.png)

# Level Blueprint

![image_3](images/image_3.png)

# Streaming Media Source

![image_4](images/image_4.png)

Streaming URL:

```
https://www.rmp-streaming.com/media/bbb-360p.mp4
```
