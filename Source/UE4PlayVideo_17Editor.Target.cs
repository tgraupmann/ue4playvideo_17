// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class UE4PlayVideo_17EditorTarget : TargetRules
{
	public UE4PlayVideo_17EditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "UE4PlayVideo_17" } );
	}
}
